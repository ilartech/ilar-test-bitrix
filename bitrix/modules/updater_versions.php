<?php
return array (
  'modules' => 
  array (
    'main' => 
    array (
      0 => 
      array (
        0 => '20.200.350',
        1 => '2021-05-25 09:50:20',
      ),
      1 => 
      array (
        0 => '20.200.400',
        1 => '2021-05-25 09:50:22',
      ),
      2 => 
      array (
        0 => '20.200.500',
        1 => '2021-05-25 09:50:24',
      ),
      3 => 
      array (
        0 => '20.200.550',
        1 => '2021-05-25 09:50:26',
      ),
      4 => 
      array (
        0 => '20.200.562',
        1 => '2021-05-25 09:50:30',
      ),
      5 => 
      array (
        0 => '20.200.575',
        1 => '2021-05-25 09:50:48',
      ),
      6 => 
      array (
        0 => '20.200.600',
        1 => '2021-05-25 09:50:57',
      ),
      7 => 
      array (
        0 => '20.200.700',
        1 => '2021-05-25 09:51:00',
      ),
      8 => 
      array (
        0 => '20.200.800',
        1 => '2021-05-25 09:51:01',
      ),
      9 => 
      array (
        0 => '20.200.900',
        1 => '2021-05-25 09:51:02',
      ),
      10 => 
      array (
        0 => '20.300.0',
        1 => '2021-05-25 09:51:29',
      ),
      11 => 
      array (
        0 => '20.400.0',
        1 => '2021-05-25 09:57:46',
      ),
      12 => 
      array (
        0 => '20.500.100',
        1 => '2021-05-25 09:58:00',
      ),
      13 => 
      array (
        0 => '20.500.150',
        1 => '2021-05-25 09:58:05',
      ),
      14 => 
      array (
        0 => '20.500.200',
        1 => '2021-05-25 09:58:10',
      ),
      15 => 
      array (
        0 => '20.500.300',
        1 => '2021-05-25 09:58:11',
      ),
      16 => 
      array (
        0 => '20.500.400',
        1 => '2021-05-25 09:58:13',
      ),
      17 => 
      array (
        0 => '20.600.0',
        1 => '2021-05-25 09:58:32',
      ),
      18 => 
      array (
        0 => '20.600.25',
        1 => '2021-05-25 09:58:36',
      ),
      19 => 
      array (
        0 => '20.600.50',
        1 => '2021-05-25 09:58:42',
      ),
      20 => 
      array (
        0 => '20.600.75',
        1 => '2021-05-25 09:58:50',
      ),
      21 => 
      array (
        0 => '20.600.87',
        1 => '2021-05-25 09:58:52',
      ),
      22 => 
      array (
        0 => '20.600.100',
        1 => '2021-05-25 09:58:57',
      ),
      23 => 
      array (
        0 => '20.700.0',
        1 => '2021-05-25 09:59:08',
      ),
    ),
    'iblock' => 
    array (
      0 => 
      array (
        0 => '20.0.1200',
        1 => '2021-05-25 09:51:35',
      ),
      1 => 
      array (
        0 => '20.0.1300',
        1 => '2021-05-25 09:51:36',
      ),
      2 => 
      array (
        0 => '20.5.0',
        1 => '2021-05-25 09:51:50',
      ),
      3 => 
      array (
        0 => '20.50.0',
        1 => '2021-05-25 09:51:52',
      ),
      4 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:52:00',
      ),
      5 => 
      array (
        0 => '20.100.100',
        1 => '2021-05-25 09:52:02',
      ),
      6 => 
      array (
        0 => '20.100.200',
        1 => '2021-05-25 09:52:04',
      ),
      7 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:52:08',
      ),
    ),
    'seo' => 
    array (
      0 => 
      array (
        0 => '20.0.500',
        1 => '2021-05-25 09:52:09',
      ),
      1 => 
      array (
        0 => '20.0.600',
        1 => '2021-05-25 09:59:29',
      ),
      2 => 
      array (
        0 => '20.0.700',
        1 => '2021-05-25 09:59:31',
      ),
      3 => 
      array (
        0 => '20.50.0',
        1 => '2021-05-25 09:59:33',
      ),
      4 => 
      array (
        0 => '20.75.0',
        1 => '2021-05-25 09:59:36',
      ),
      5 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:59:45',
      ),
    ),
    'socialservices' => 
    array (
      0 => 
      array (
        0 => '20.0.600',
        1 => '2021-05-25 09:52:11',
      ),
      1 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:52:13',
      ),
    ),
    'clouds' => 
    array (
      0 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:52:15',
      ),
    ),
    'highloadblock' => 
    array (
      0 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:52:17',
      ),
    ),
    'b24connector' => 
    array (
      0 => 
      array (
        0 => '20.5.100',
        1 => '2021-05-25 09:52:18',
      ),
    ),
    'messageservice' => 
    array (
      0 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:52:20',
      ),
    ),
    'ui' => 
    array (
      0 => 
      array (
        0 => '20.5.900',
        1 => '2021-05-25 09:52:29',
      ),
      1 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:52:39',
      ),
      2 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:52:50',
      ),
      3 => 
      array (
        0 => '20.250.0',
        1 => '2021-05-25 09:53:52',
      ),
      4 => 
      array (
        0 => '20.300.0',
        1 => '2021-05-25 09:57:46',
      ),
      5 => 
      array (
        0 => '20.400.0',
        1 => '2021-05-25 10:00:23',
      ),
      6 => 
      array (
        0 => '20.500.0',
        1 => '2021-05-25 10:01:06',
      ),
      7 => 
      array (
        0 => '20.500.100',
        1 => '2021-05-25 10:01:11',
      ),
      8 => 
      array (
        0 => '20.500.200',
        1 => '2021-05-25 10:01:14',
      ),
      9 => 
      array (
        0 => '20.600.0',
        1 => '2021-05-25 10:01:25',
      ),
    ),
    'rest' => 
    array (
      0 => 
      array (
        0 => '20.50.200',
        1 => '2021-05-25 09:52:53',
      ),
      1 => 
      array (
        0 => '20.50.300',
        1 => '2021-05-25 09:52:58',
      ),
      2 => 
      array (
        0 => '20.50.400',
        1 => '2021-05-25 09:53:00',
      ),
      3 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:53:09',
      ),
      4 => 
      array (
        0 => '20.100.100',
        1 => '2021-05-25 09:53:11',
      ),
      5 => 
      array (
        0 => '20.150.0',
        1 => '2021-05-25 09:53:12',
      ),
      6 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:53:17',
      ),
      7 => 
      array (
        0 => '20.200.100',
        1 => '2021-05-25 09:53:18',
      ),
      8 => 
      array (
        0 => '20.200.200',
        1 => '2021-05-25 09:53:20',
      ),
      9 => 
      array (
        0 => '20.200.300',
        1 => '2021-05-25 09:53:22',
      ),
    ),
    'landing' => 
    array (
      0 => 
      array (
        0 => '20.4.855',
        1 => '2021-05-25 09:53:54',
      ),
      1 => 
      array (
        0 => '20.5.0',
        1 => '2021-05-25 09:55:34',
      ),
      2 => 
      array (
        0 => '20.5.50',
        1 => '2021-05-25 09:55:52',
      ),
      3 => 
      array (
        0 => '20.5.100',
        1 => '2021-05-25 09:56:13',
      ),
      4 => 
      array (
        0 => '20.5.200',
        1 => '2021-05-25 09:56:18',
      ),
      5 => 
      array (
        0 => '20.5.300',
        1 => '2021-05-25 09:56:27',
      ),
      6 => 
      array (
        0 => '20.5.400',
        1 => '2021-05-25 09:56:34',
      ),
      7 => 
      array (
        0 => '20.5.500',
        1 => '2021-05-25 09:56:37',
      ),
      8 => 
      array (
        0 => '20.5.512',
        1 => '2021-05-25 09:56:42',
      ),
      9 => 
      array (
        0 => '20.5.525',
        1 => '2021-05-25 10:01:28',
      ),
      10 => 
      array (
        0 => '20.5.537',
        1 => '2021-05-25 10:01:31',
      ),
      11 => 
      array (
        0 => '20.5.550',
        1 => '2021-05-25 10:01:38',
      ),
    ),
    'fileman' => 
    array (
      0 => 
      array (
        0 => '20.100.0',
        1 => '2021-05-25 09:57:46',
      ),
      1 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:59:11',
      ),
      2 => 
      array (
        0 => '20.200.100',
        1 => '2021-05-25 09:59:13',
      ),
      3 => 
      array (
        0 => '20.200.200',
        1 => '2021-05-25 09:59:16',
      ),
      4 => 
      array (
        0 => '20.200.300',
        1 => '2021-05-25 09:59:19',
      ),
      5 => 
      array (
        0 => '20.300.0',
        1 => '2021-05-25 09:59:23',
      ),
    ),
    'perfmon' => 
    array (
      0 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 09:59:25',
      ),
    ),
    'translate' => 
    array (
      0 => 
      array (
        0 => '20.200.0',
        1 => '2021-05-25 10:01:10',
      ),
    ),
  ),
);
