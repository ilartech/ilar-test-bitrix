$(document).ready(function () {
    sortCatalog();
    initForms();
    updateProductForm();
    addOptionToForm();
});

function sortCatalog() {
    $('body').on('change', '.js__sort', function (event) {
        var sort = $(this).val();
        var block = '.ajax__content';
        $.ajax({
            type: "POST",
            data: {
                sort: sort
            },
            beforeSend: function () {
                $(block).fadeTo(200, 0.2)
            },
            success: function (res) {
                var text = $(res).find(block).html()
                $(block).html(text);
                $(block).fadeTo(200, 1)
            }
        });
    });
}

function initForms() {
    var body = $('body');
    body.on('submit', '.ajax__form', function (event) {
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var email = elem.find('input[type=email]').val();
        if (email === undefined) {
            email = 'test@test.ru';
        }
        var data = elem.serialize();
        if (elem.hasClass('form__file')) {
            data = new FormData(elem[0]);
            $.ajax({
                type: "POST",
                cache: false,
                processData: false,
                contentType: false,
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                    } else {
                    }
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                    }
                }
            });
        }
    });
}

function updateProductForm() {
    $('body').on('click', '.js__product_form', function () {
        var productName = $(this).attr('data-name');
        var productId = $(this).attr('data-id');
        var productPrice = $(this).attr('data-price');
        $('#js__product-name').text(productName);
        $('#js__product-price').text(productPrice + ' ₽');
        $('#js__product-id').val(productId);
    })
}

function addOptionToForm() {
    $('body').on('change', '.js__option', function () {
        var checked = $(this).prop('checked');
        var key = $(this).attr('data-element');
        var val = $(this).val();
        var inputHtml = '<input class="input-default" type="hidden" name="option[]" value="' + val + '" data-element="' + key + '">';
        var priceTotal = $('.js__price').attr('data-price');
        var priceCurrent = $(this).attr('data-price');
        if (checked) {
            $('#popup-card').find('form').prepend(inputHtml);
            var price = parseInt(priceTotal) + parseInt(priceCurrent);
        } else {
            var price = parseInt(priceTotal) - parseInt(priceCurrent);
            $('#popup-card').find('form').find('input[data-element="' + key + '"]').remove();
        }
        $('.js__price').attr('data-price', price);
        $('.js__price').text(price.toLocaleString('de-DE').replace(/\./g, " "));
    })
}