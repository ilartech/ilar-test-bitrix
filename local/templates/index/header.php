<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/templates/head.php', [], ['SHOW_BORDER' => false]) ?>
    <? $APPLICATION->ShowHead(); ?>
</head>

<body class="page" data-temp="index">
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/templates/header.php', [], ['SHOW_BORDER' => false]) ?>
<section class="page__wrapper">
    <? if($APPLICATION->GetCurPage() != '/'): ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumb",
            [
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
                "COMPONENT_TEMPLATE" => "breadcrumb",
            ],
            false
        );?>
        <div class="tyre__title-wrapper container">
            <div class="title-default title-default--border"><? $APPLICATION->ShowTitle(false); ?></div>
        </div>
    <? endif; ?>