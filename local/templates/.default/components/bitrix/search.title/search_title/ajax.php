<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!empty($arResult["CATEGORIES"]) && $arResult['CATEGORIES_ITEMS_EXISTS']):?>
    <? foreach ($arResult["CATEGORIES"] as $category_id => $arCategory): ?>
        <? if ($category_id === 'all') continue ?>
        <ul class="result_list">
            <? foreach ($arCategory["ITEMS"] as $i => $arItem): ?>
                <? if ($arItem['TYPE'] != 'all'): ?>
                    <li>
                        <a href="<?= $arItem['URL'] ?>"><?= $arItem['NAME'] ?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
    <? endforeach; ?>
<?endif;
?>