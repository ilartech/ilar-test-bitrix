<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arResult
 * @var array $arParam
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/grid/pagenavigation.css");

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
?>
<?php if($arResult['NavPageCount'] > 1): ?>
<div class="pagination">
    <div class="pagination__steps">
        <?
        $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
        $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
        ?>

        <?if ($arResult["bDescPageNumbering"] === true):?>
            <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                if ($arResult["bSavePage"]):?>
                    <a class="button pagination__next"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                        <span>Вперед</span>
                    </a>
                <?else:
                    if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                            <span>Вперед</span>
                        </a>
                    <?else:?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                            <span>Вперед</span>
                        </a>
                    <?endif;
                endif;
            else:?>
                <span class="button pagination__next">
                    <span>Вперед</span>
                </span>
            <?endif; ?>
            <?php if ($arResult["NavPageNomer"] > 1):?>
                <a class="button pagination__prev"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                    <span>Назад</span>
                </a>
            <?else:?>
                <span class="button pagination__prev">
                        <span>Назад</span>
                    </span>
            <?endif;?>
            <div class="pagination__pages">
                <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                    if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                        if ($arResult["bSavePage"]):?>
                            <a class="pagination__step"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a>
                        <?else:?>
                            <a class="pagination__step"
                               href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                        <?endif;
                        if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):?>
                            <a class="pagination__dots"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2) ?>">...</a>
                        <?endif;
                    endif;
                endif;

                do {
                    $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <span class="pagination__step pagination__step--active"><?= $NavRecordGroupPrint ?></span>
                    <?elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
                        <a class="pagination__step"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $NavRecordGroupPrint ?></a>
                    <?else:?>
                        <a class="pagination__step"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $NavRecordGroupPrint ?></a>
                    <?endif;

                    $arResult["nStartPage"]--;
                } while ($arResult["nStartPage"] >= $arResult["nEndPage"]);

                if ($arResult["NavPageNomer"] > 1):
                    if ($arResult["nEndPage"] > 1):
                        if ($arResult["nEndPage"] > 2):?>
                            <a class="pagination__step pagination__page pagination__page-dots"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] / 2) ?>">...</a>
                        <?endif;?>
                        <a class="pagination__step"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>
                    <?endif;
                endif;?>
            </div>
            <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                if ($arResult["bSavePage"]):?>
                    <a class="button pagination__next"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                        <span>Вперед</span>
                    </a>
                <?else:
                    if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                            <span>Вперед</span>
                        </a>
                    <?else:?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                            <span>Вперед</span>
                        </a>
                    <?endif;
                endif;
            else:?>
                <span class="button pagination__next">
                    <span>Вперед</span>
                </span>
            <?endif; ?>
        <?else:?>
            <?php if ($arResult["NavPageNomer"] > 1):?>
                <a class="button pagination__prev"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                    <span>Назад</span>
                </a>
            <?else:?>
                <span class="button pagination__prev">
                        <span>Назад</span>
                    </span>
            <?endif;?>
            <div class="pagination__pages">
                <div class="pagination__steps">
                    <?if ($arResult["NavPageNomer"] > 1):
                        if ($arResult["nStartPage"] > 1):
                            if ($arResult["bSavePage"]):?>
                                <a class="pagination__step"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
                            <?else:?>
                                <a class="pagination__step"
                                   href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                            <?endif;
                            if ($arResult["nStartPage"] > 2):?>
                                <a class="pagination__step pagination__page pagination__page-dots"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>">...</a>
                            <?endif;
                        endif;
                    endif;

                    do {
                        if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                            <span class="pagination__step pagination__step--active"><?= $arResult["nStartPage"] ?></span>
                        <?elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
                            <a class="pagination__step"
                               href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
                        <?else:?>
                            <a class="pagination__step"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
                        <?endif;
                        $arResult["nStartPage"]++;
                    } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

                    if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                            if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):?>
                                <a class="pagination__step pagination__page pagination__page-dots"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>">...</a>
                            <?endif;?>
                            <a class="pagination__step"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
                        <?endif;
                    endif;
                    ?>
                </div>
            </div>
            <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                if ($arResult["bSavePage"]):?>
                    <a class="button pagination__next"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                        <span>Вперед</span>
                    </a>
                <?else:
                    if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                            <span>Вперед</span>
                        </a>
                    <?else:?>
                        <a class="button pagination__next"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                            <span>Вперед</span>
                        </a>
                    <?endif;
                endif;
            else:?>
                <span class="button pagination__next">
                    <span>Вперед</span>
                </span>
            <?endif; ?>
        <?endif;?>
    </div>
</div>
<?php endif; ?>