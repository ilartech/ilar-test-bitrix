<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="footer__nav-items footer__nav-desc">
        <? foreach ($arResult as $arItem): ?>
            <div class="toggle toggle-disable">
                <div class="toggle__title <?= ($arItem['ITEMS'] ? 'toggle__title--with-arrow' : false) ?>">
                    <a href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                </div>
                <? if ($arItem['ITEMS']): ?>
                    <div class="toggle__content">
                        <div class="mobile-menu__item-list">
                            <? foreach ($arItem['ITEMS'] as $arItemL2): ?>
                                <a href="<?= $arItemL2['LINK'] ?>"><?= $arItemL2['TEXT'] ?></a>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        <? endforeach; ?>
    </div>
<? endif ?>