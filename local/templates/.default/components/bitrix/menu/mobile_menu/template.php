<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <div class="mobile-menu mobile">
        <div class="mobile-menu__close"></div>
        <? foreach ($arResult as $arItem): ?>
            <div class="mobile-menu__item toggle">
                <div class="mobile-menu__title toggle__title">
                    <a href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                    <? if ($arItem['ITEMS']): ?>
                        <svg class="customClass" width="12" height="12">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#2201"></use>
                        </svg>
                    <? endif; ?>
                </div>
                <? if ($arItem['ITEMS']): ?>
                    <div class="mobile-menu__content toggle__content">
                        <ul class="mobile-menu__item-list mobile-menu-item-list">
                            <? foreach ($arItem['ITEMS'] as $arItemL2): ?>
                                <li class="mobile-menu-item-list__item">
                                    <a href="#"><?= $arItemL2['TEXT'] ?></a>
                                    <? if ($arItemL2['ITEMS']): ?>
                                        <ul class="submenu">
                                            <? foreach ($arItemL2['ITEMS'] as $arItemL3): ?>
                                                <li class="submenu__item">
                                                    <a class="submenu__link"
                                                       href="<?= $arItemL3['LINK'] ?>"><?= $arItemL3['TEXT'] ?></a>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif; ?>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                <? endif; ?>
            </div>
        <? endforeach; ?>
    </div>
<? endif ?>