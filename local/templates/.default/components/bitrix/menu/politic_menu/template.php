<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <span><?= GetMessage('COPYRIGHT') ?></span>
<? if (!empty($arResult)): ?>
    <? foreach ($arResult as $arItem): ?>
        <a href="<?= $arItem['LINK'] ?>"
            <?= (substr($arItem['LINK'], 0, 4) === "http" ? 'target="_blank"' : false) ?>><?= $arItem['TEXT'] ?></a>
    <? endforeach; ?>
<? endif ?>