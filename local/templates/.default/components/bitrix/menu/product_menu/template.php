<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <div class="card__services card-services">
        <ul class="card-services__list">
            <? foreach ($arResult as $arItem): ?>
                <li class="card-services__item">
                    <a class="card-services__link" href="<?= $arItem['LINK'] ?>">
                        <svg class="customClass" width="75" height="75">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#<?= $arItem['PARAMS']['ICON'] ?>"></use>
                        </svg>
                        <p class="card-services__text"><?= $arItem['TEXT'] ?></p>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
<? endif ?>