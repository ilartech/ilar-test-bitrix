<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <ul class="topmenu header__navigation">
        <? foreach ($arResult as $arItem): ?>
            <li class="topmenu__item topmenu__item--top-row <?= ($arItem['ITEMS'] ? 'topmenu__item--with-arrow' : false) ?>">
                <a class="topmenu__link <?= ($arItem['SELECTED'] ? 'active' : false) ?>" href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                <? if ($arItem['ITEMS']): ?>
                    <ul class="submenu">
                        <? foreach ($arItem['ITEMS'] as $arItemL2): ?>
                            <li class="submenu__item">
                                <a class="submenu__link" href="<?= $arItemL2['LINK'] ?>"><?= $arItemL2['TEXT'] ?></a>
                                <? if($arItemL2['ITEMS']): ?>
                                <ul class="submenu">
                                    <? foreach ($arItemL2['ITEMS'] as $arItemL3): ?>
                                    <li class="submenu__item">
                                        <a class="submenu__link" href="<?= $arItemL3['LINK'] ?>"><?= $arItemL3['TEXT'] ?></a>
                                    </li>
                                    <?endforeach; ?>
                                </ul>
                                <? endif; ?>
                            </li>
                        <? endforeach; ?>
                    </ul>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
<? endif ?>