<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="footer__nav-items footer__nav-mob">
        <? foreach ($arResult as $arItem): ?>
            <div class="toggle">
                <div class="toggle__title">
                    <a href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                    <? if ($arItem['ITEMS']): ?>
                        <svg class="customClass" width="12" height="12">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#2201"></use>
                        </svg>
                    <? endif; ?>
                </div>
                <? if ($arItem['ITEMS']): ?>
                    <div class="toggle__content">
                        <div class="mobile-menu__item-list">
                            <? foreach ($arItem['ITEMS'] as $arItemL2): ?>
                                <a href="<?= $arItemL2['LINK'] ?>"><?= $arItemL2['TEXT'] ?></a>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        <? endforeach; ?>
    </div>
<? endif ?>
