<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
    'width'  => 1320,
    'height' => 1320,
    'exact'  => 0,
    //'jpg_quality' => 75
]
);
?><? if ($image): ?>
    <img alt="<?= $image['DESCRIPTION'] ?>" src="<?= $image['SRC'] ?>">
<? endif; ?>
