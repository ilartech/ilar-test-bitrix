<? /** @var $block array */ ?>
<div class="card__btn-wrapper">
    <a class="button button--basket button--hover popup-js button--delivery-price" href="<?= $block['link'] ?>" rel="nofollow" >
        <span><?= $block['title'] ?></span>
    </a>
</div>