<?
define("SITE_STYLE_PATH", "/local/styles");
define("SITE_INCLUDE_PATH", "/local/include");
define("SITE_USER_CLASS_PATH", "/local/php_interface/user_class");
define("SITE_AJAX_PATH", "/local/ajax");
define("IBLOCK_CATALOG_ID", 1);
define("IBLOCK_FORM_DELIVERY", 5);
define("IBLOCK_FORM_CATALOG", 6);

require $_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php";