<?

namespace IL;

use \Bitrix\Main\Loader;

/**
 * Класс для работы с формой заявка на услугу
 * Class FormFeedback
 * @package IL
 */
class AddProduct extends Iblock
{

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @return bool|string
     */
    public function add($arFields)
    {
        if (!Loader::includeModule('iblock')) return false;
        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "NAME" => $arFields['name'],
            "CODE" => self::translit(strtolower($arFields['name'])),
            "DETAIL_TEXT" => $arFields['description'],
            "PROPERTY_VALUES" => [
                'TITLE' => $arFields['title'],
                'ARTICLE' => $arFields['vendor'],
                'PRICE' => preg_replace('~\D+~', '', $arFields['price']['price']),
                'OLD_PRICE' => preg_replace('~\D+~', '', $arFields['price']['old_price']),
                'TERMS' => $arFields['term'],
                'PREVIEW_PROPS' => [],
                'SPECIFICATIONS' => [],
                'OPTIONS' => [],
            ],
            "ACTIVE" => "Y",
            "PREVIEW_PICTURE" => \CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFields['images'][0]),
            "DETAIL_PICTURE" => \CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFields['images'][0]),
            "TYPE" => "html",
        ];
        $index = 0;
        foreach ($arFields['preview'] as $value => $description) {
            $arLoadProductArray['PROPERTY_VALUES']['PREVIEW_PROPS']['n' . $index] = [
                "VALUE" => $value,
                "DESCRIPTION" => $description,
            ];
            $value = mb_strtolower(preg_replace('/\PL/u', '', $value));
            switch ($value) {
                case 'номинальная':
                    $prop = 'POWER';
                    $propID = 7;
                    break;
                case 'двигатель':
                    $prop = 'ENGINE';
                    $propID = 8;
                    break;
                case 'напряжение':
                    $prop = 'VOLTAGE';
                    $propID = 9;
                    break;
                case 'исполнение':
                    $prop = 'EXECUTION';
                    $propID = 10;
                    break;
                case 'автоматизация':
                    $prop = 'AUTOMATION';
                    $propID = 11;
                    break;
            }
            if ($prop) {
                if ($prop == 'POWER') {
                    $arDescription = explode('кВт', $description);
                    $description = $arDescription[0];
                    $arLoadProductArray['PROPERTY_VALUES'][$prop] = ['VALUE' => $description];
                } else {
                    $property_enums = \CIBlockPropertyEnum::GetList(
                        ["DEF" => "DESC", "SORT" => "ASC"],
                        ["IBLOCK_ID" => $this->iblockID, "PROPERTY_ID" => $propID, "VALUE" => $description])->Fetch();
                    if (!$property_enums['ID']) {
                        $ibpenum = new \CIBlockPropertyEnum;
                        $property_enums['ID'] = $ibpenum->Add(["IBLOCK_ID" => $this->iblockID, "PROPERTY_ID" => $propID, "VALUE" => $description]);
                    }
                    $arLoadProductArray['PROPERTY_VALUES'][$prop] = ['VALUE' => $property_enums['ID']];
                }
            }
            unset($prop);
            $index++;
        }
        $index = 0;
        foreach ($arFields['tables'] as $table) {
            $arTable = self::arrayToTable($table);
            $arLoadProductArray['PROPERTY_VALUES']['SPECIFICATIONS']['n' . $index] = [
                "VALUE" => $arTable['html'],
                "DESCRIPTION" => $arTable['name'],
            ];
            $index++;
        }
        $index = 0;
        foreach ($arFields['options'] as $option) {
            $arLoadProductArray['PROPERTY_VALUES']['OPTIONS']['n' . $index] = [
                "VALUE" => $option['name'],
                "DESCRIPTION" => preg_replace('~\D+~', '', $option['value']),
            ];
            $index++;
        }
        $element = \IL\Catalog::getElementList($this->iblockID, ['NAME' => $arFields['name']])[0];
        if ($element['ID']) {
            $el->Update($element['ID'], $arLoadProductArray);
        } else {
            if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                $arResult = [
                    'status' => 'ok',
                    'title' => 'Элемент успешно добавлен ' . $PRODUCT_ID,
                ];
            } else {
                $arResult = [
                    'status' => 'error',
                    'message' => $el->LAST_ERROR,
                ];
            }
        }
        return $arResult;
    }

    public function arrayToTable($array)
    {
        $html = '';
        $arTable = [];
        $arTable['name'] = $array['name'];
        $html .= '<table>';
        foreach ($array['params'] as $tr) {
            if (gettype($tr['value']) == 'string') {
                $html .= '<tr>';
                $html .= '<td>' . $tr['name'] . '</td>';
                $html .= '<td>' . $tr['value'] . '</td>';
                $html .= '</tr>';
            } elseif (gettype($tr['value']) == 'array') {
                $html .= '<tr>';
                $html .= '<td colspan="2">' . $tr['name'] . '</td>';
                $html .= '</tr>';
                foreach ($tr['value'] as $td) {
                    $html .= '<tr>';
                    $html .= '<td>' . $td['name'] . '</td>';
                    $html .= '<td>' . $td['value'] . '</td>';
                    $html .= '</tr>';
                }
            }
        }
        $html .= '</table>';
        $arTable['html'] = $html;
        return $arTable;
    }

    public function translit($value)
    {
        $converter = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', ' ' => '_',
        ];


        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '-', $value);
        $value = trim($value, '-');

        return $value;
    }
}