sprint_editor.registerBlock('my_big_link', function ($, $el, data, settings) {

    settings = settings || {};

    data = $.extend({
        title: '',
        link: '',
    }, data);

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.title = $el.find('.sp-input[name="title"]').val();
        data.link = $el.find('.sp-input[name="link"]').val();
        return data;
    };

    this.afterRender = function () {
    };
});
