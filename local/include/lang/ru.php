<?
$MESS["PHONE_CONSULTATION"] = "Консультация по телефону";
$MESS["SEARCH_ON_SITE"] = "Поиск по сайту";
$MESS["YOUR_SEARCH"] = "Вы искали";
$MESS["COINCIDENCE1"] = "совпадение";
$MESS["COINCIDENCE2"] = "совпадения";
$MESS["COINCIDENCE5"] = "совпадений";
$MESS["SEARCH_FAIL_TITLE"] = "К сожалению по вашему запросу нет совпадений";
$MESS["SEARCH_FAIL_SUBTITLE"] = "Попробуйте ввести ваш запрос еще раз";
$MESS["ARTICLE"] = "Артикул";
$MESS["BUY"] = "Купить";
$MESS["POPULAR_GOODS"] = "Популярные товары";
$MESS["DISPLAY_TYPE"] = "Вид отображения";
$MESS["SORT_BY"] = "Сортировать по";
$MESS["FILTER"] = "Фильтр";
$MESS["CONTACTS"] = "Контакты";
$MESS["EMAIL"] = "Электронная почта";
$MESS["CITY"] = "Город";
$MESS["PHONE"] = "Телефон";
$MESS["ACCEPT_PAYMENT"] = "К оплате принимаем";
$MESS["COPYRIGHT"] = "© 2003-2018 «КазаньШинТорг»";
$MESS["FILTER_TITLE"] = "Дизельные генераторы";
$MESS["CHOOSE"] = "Выбрать";
$MESS["PRICE_TITLE"] = "Цена за шт";
$MESS["ADD_OPTIONS"] = "Дополнительные опции";
$MESS["CUSTOMER_SERVICE"] = "Сервисная служба";
$MESS["CUSTOMER_SERVICE_DESC"] = "Если у вас была проблема с заказом или другая проблема, вы можете обратиться за помощью по следующим контактам:";
$MESS["FORM_SEND"] = "Отправить";
$MESS["FORM_AGREEMENT"] = "- Я согласен на обработку моих <br> <a href='/privacy/'>персональных данных</a>";
$MESS["FORM_PHONE"] = "Номер телефона";
$MESS["FORM_NAME"] = "Имя";
$MESS["FORM_DELIVERY_TITLE"] = "Оставьте ваши контактные данные и наш менеджер рассчитает стоимость доставки до вашего города";
$MESS["MORE_CHARACTERISTICS"] = "Подробные технические характеристики";
$MESS["MOVE_TO_BACK"] = "Вернуться назад";
$MESS["FORM_MESSAGE"] = "Комментарий к заказу";
$MESS["FORM_SEND_ORDER"] = "Отправить заказ";
$MESS["THX_ORDER"] = "Спасибо за заявку";
$MESS["THX_ORDER_DESC"] = "Наш менеджер свяжется с вами в ближайшее время";
$MESS["MOVE_TO_CATALOG"] = "Перейти в каталог";
$MESS["PAYMENTS"] = "К оплате принимаем";
$MESS["SIMILAR_GOODS"] = "Похожие товары";