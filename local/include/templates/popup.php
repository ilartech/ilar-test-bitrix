<div class="mfp-hide white-popup popup-filter popup popup-click popup-card" id="popup-card">
    <div class="card">
        <div class="card__head">
            <div class="card__img">
                <img src="<?= SITE_STYLE_PATH ?>/img/general/Image_50.png">
            </div>
        </div>
        <div class="card__main">
            <div class="card__options-wrapper card__options-wrapper--popup">
                <div class="card__title" id="js__product-name"></div>
                <div class="card__price card__price--popup" id="js__product-price"></div>
            </div>
            <form action="<?= SITE_AJAX_PATH ?>/form.php"
                  method="post" enctype="multipart/form-data" class="ajax__form">
                <input type="hidden" name="iblock_id" value="<?= IBLOCK_FORM_CATALOG ?>">
                <input type="hidden" name="binding" id="js__product-id">
                <div class="input__row">
                    <div class="input">
                        <div class="input__title"><?= GetMessage('FORM_NAME') ?> <span>*</span>
                        </div>
                        <label>
                            <input class="input-default" type="text" name="name">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('FORM_PHONE') ?> <span>*</span>
                        </div>
                        <label>
                            <input class="input-default phone" type="tel" name="phone">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('CITY') ?></div>
                        <label>
                            <input class="input-default" type="text" name="city">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('EMAIL') ?></div>
                        <label>
                            <input class="input-default" type="email" name="email">
                        </label>
                    </div>
                    <div class="input input--comment">
                        <div class="input__title"><?= GetMessage('FORM_MESSAGE') ?></div>
                        <label>
                            <textarea class="input-default input-default--textarea" type="text" cols="30" rows="3" name="message"></textarea>
                        </label>
                    </div>
                    <div class="checkbox__row">
                        <div class="checkbox">
                            <label class="label">
                                <input type="checkbox" name="type">
                                <div class="checkbox-button"></div>
                                <span><?= GetMessage('FORM_AGREEMENT') ?></span>
                            </label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="button button--basket button--hover button--popup">
                    <span><?= GetMessage('FORM_SEND_ORDER') ?></span>
                </button>
            </form>
        </div>
    </div>
</div>
<div class="mfp-hide white-popup popup-filter popup popup-click popup-card" id="popup-deliv">
    <div class="card">
        <div class="card__main">
            <div class="card__options-wrapper card__options-wrapper--popup">
                <div class="card__title card__title--popup-deliv"><?= GetMessage('FORM_DELIVERY_TITLE') ?></div>
            </div>
            <form action="<?= SITE_AJAX_PATH ?>/form.php"
                  method="post" enctype="multipart/form-data" class="ajax__form">
                <input type="hidden" name="iblock_id" value="<?= IBLOCK_FORM_DELIVERY ?>">
                <div class="input__row">
                    <div class="input">
                        <div class="input__title"><?= GetMessage('FORM_NAME') ?><span>*</span></div>
                        <label>
                            <input class="input-default" type="text" name="name">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('FORM_PHONE') ?> <span>*</span></div>
                        <label>
                            <input class="input-default phone" type="tel" name="phone">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('CITY') ?></div>
                        <label>
                            <input class="input-default" type="text" name="city">
                        </label>
                    </div>
                    <div class="input">
                        <div class="input__title"><?= GetMessage('EMAIL') ?></div>
                        <label>
                            <input class="input-default" type="email" name="email">
                        </label>
                    </div>
                    <div class="checkbox__row">
                        <div class="checkbox">
                            <label class="label">
                                <input type="checkbox" name="type">
                                <div class="checkbox-button"></div>
                                <span><?= GetMessage('FORM_AGREEMENT') ?></span>
                            </label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="button button--basket button--hover button--popup">
                    <span><?= GetMessage('FORM_SEND') ?></span>
                </button>
            </form>
        </div>
    </div>
</div>
<? // TODO success ?>
<div class="white-popup mfp-hide popup-filter popup popup-click popup-thx" id="popup-thx">
    <h3 class="popup-click__title"><?= GetMessage('THX_ORDER') ?></h3>
    <div class="popup-click__text"><?= GetMessage('THX_ORDER_DESC') ?></div>
    <a href="/catalog/">
        <div class="button button--hover button--basket">
            <span><?= GetMessage('MOVE_TO_CATALOG') ?></span>
        </div>
    </a>
</div>