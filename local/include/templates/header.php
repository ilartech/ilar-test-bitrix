<div class="header">
    <div class="header__contacts tablet">
        <div class="container container--header">
            <div class="header__burger burger burger--open mobile"></div>
            <div class="header__logo header__logo--contacts">
                <div class="header__logo-img">
                    <a href="<?= SITE_DIR ?>">
                        <svg class="customClass" width="40" height="33">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#logo"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="header__phone phone-block">
                <div class="phone-block__title"><?= GetMessage('PHONE_CONSULTATION') ?>:</div>
                <a class="phone-block__current"
                   href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")); ?>"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE"); ?></a>
            </div>
        </div>
    </div>
    <div class="header__top">
        <div class="container container_header-top container_header-top--mobile">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top_menu",
                [
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "3",
                    "MENU_CACHE_GET_VARS" => [""],
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                ]
            ); ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:search.title",
                "search_title",
                array(
                    "CATEGORY_0" => array(
                        0 => "iblock_catalog",
                    ),
                    "CATEGORY_0_TITLE" => "",
                    "CHECK_DATES" => "N",
                    "CONTAINER_ID" => "title-search",
                    "INPUT_ID" => "title-search-input",
                    "NUM_CATEGORIES" => "1",
                    "ORDER" => "date",
                    "PAGE" => "#SITE_DIR#search/",
                    "SHOW_INPUT" => "Y",
                    "SHOW_OTHERS" => "N",
                    "TOP_COUNT" => "5",
                    "USE_LANGUAGE_GUESS" => "Y",
                    "COMPONENT_TEMPLATE" => "search_title",
                    "CATEGORY_0_iblock_catalog" => array(
                        0 => "1",
                    )
                ),
                false
            );?>
        </div>
    </div>
</div>
<? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "mobile_menu",
    [
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "3",
        "MENU_CACHE_GET_VARS" => [""],
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "top",
        "USE_EXT" => "Y",
    ]
); ?>