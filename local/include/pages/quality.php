<div class="quality">
    <div class="quality__title"><?= GetMessage('CUSTOMER_SERVICE') ?></div>
    <div class="quality__text"><?= GetMessage('CUSTOMER_SERVICE_DESC') ?></div>
    <div class="phone-block">
        <div class="phone-block__title"><?= GetMessage('PHONE') ?></div>
        <a class="phone-block__current"
           href="tel:<?= preg_replace('~\D+~', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")); ?>"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE") ?></a>
    </div>
    <div class="phone-block phone-block--mail">
        <div class="phone-block__title"><?= GetMessage('EMAIL') ?></div>
        <a class="phone-block__current"
           href="mailto:<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL") ?>"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL") ?></a>
    </div>
</div>