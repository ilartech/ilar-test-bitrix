<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $iblockID = $_POST['iblock_id'];
    $obForm = new \IL\Form($iblockID);
    $arFields = $_POST;
    if (!empty($_FILES['file']['name'][0])) {
        $arFields['FILES'] = $_FILES;
    }
    $arResult = $obForm->add($arFields);
}