<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$arProduct = json_decode($_REQUEST['product'], true);
$iblockID = 1;
$obForm = new \IL\AddProduct($iblockID);
$arResult = $obForm->add($arProduct);

echo json_encode($arResult);
