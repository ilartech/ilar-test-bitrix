<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
\Bitrix\Main\Loader::includeModule("iblock");
$iblockID = 1;
$arPropertyName = array(
    'POWER' => 'Мощность',
    'EXECUTION' => 'Исполнение',
    'ENGINE' => 'Двигатель',
);


/* ----- Каталог ----- */
$arResult = [];
$arParam = [];
$arParam["IBLOCK_ID"] = $iblockID;
$arParam["DEPTH_LEVEL"] = 1;

$sectionProperty = [];

$facet = new \Bitrix\Iblock\PropertyIndex\Facet($arParam["IBLOCK_ID"]);
//$facet->setSectionId($arSection["ID"]);
$res = $facet->query(["ACTIVE" => "Y", "ACTIVE_DATE" => "Y", "CHECK_PERMISSIONS" => "Y"]);
while ($row = $res->fetch()) {
    if ($row['ELEMENT_COUNT'] == 0) continue;
    $facetId = $row["FACET_ID"];
    if (\Bitrix\Iblock\PropertyIndex\Storage::isPropertyId($facetId)) {
        $PID = \Bitrix\Iblock\PropertyIndex\Storage::facetIdToPropertyId($facetId);
        if ($PID == "7"){ // ID свойства Мощность
            $enum = CIBlockPropertyEnum::GetByID($row["VALUE"]);
            if ($enum){
                $sectionProperty["POWER"][$row["VALUE"]] = $enum;
                $sectionProperty["POWER"][$row["VALUE"]]['ELEMENT_COUNT'] = $row['ELEMENT_COUNT'];
            }
        }

        if ($PID == "10"){ // ID свойства Исполнение
            $enum = CIBlockPropertyEnum::GetByID($row["VALUE"]);
            if ($enum){
                $sectionProperty["EXECUTION"][$row["VALUE"]] = $enum;
                $sectionProperty["EXECUTION"][$row["VALUE"]]['ELEMENT_COUNT'] = $row['ELEMENT_COUNT'];
            }
        }

        if ($PID == "8"){ // ID свойства Двигатель
            $enum = CIBlockPropertyEnum::GetByID($row["VALUE"]);
            if ($enum){
                $sectionProperty["ENGINE"][$row["VALUE"]] = $enum;
                $sectionProperty["ENGINE"][$row["VALUE"]]['ELEMENT_COUNT'] = $row['ELEMENT_COUNT'];
            }
        }
    }
}

foreach ($sectionProperty as $code => $arProps) {
    $arResult['SECTIONS'][$index] = [
        "ID" => $arSection["ID"] . '_' . $code,
        "~NAME" => $arPropertyName[$code],
        "DEPTH_LEVEL" => 2,
        "~SECTION_PAGE_URL" => "",
        "COL" => "Y",
        "UF_MENU_TYPE" => $arSection['UF_MENU_TYPE'],
        "DETAIL_PICTURE" => $arSection['DETAIL_PICTURE'],
    ];
    $index++;

    //------------------ СОРТИРОВКА -----------------------
    $sort = [];
    foreach ($arProps as $arOneValue) {
        $sort[] = mb_strtolower($arOneValue['VALUE']);
    }
    array_multisort($arProps, SORT_STRING, $sort);
    //-----------------------------------------------------

    foreach ($arProps as $arPropValue) {
        $arResult['SECTIONS'][$index] = [
            "ID" => $arSection["ID"] . '_' . $code . '_' . $arPropValue["ID"],
            "~NAME" => $arPropValue["VALUE"],
            "DEPTH_LEVEL" => 3,
            "~SECTION_PAGE_URL" => '/catalog/filter/' . mb_strtolower($code) . '-is-' . CUtil::translit(toLower($arPropValue["VALUE"]), "ru") . '/apply/',
        ];
        $index++;
    }
}

/* ----- Результирующий массив ----- */
$aMenuLinksNew = [];
$menuIndex = 0;
$previousDepthLevel = 1;
foreach ($arResult["SECTIONS"] as $arSection) {

    if ($menuIndex > 0)
        $aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
    $previousDepthLevel = $arSection["DEPTH_LEVEL"];

    $arResult["ELEMENT_LINKS"][$arSection["ID"]][] = urldecode($arSection["~SECTION_PAGE_URL"]);


    $aMenuLinksNew[$menuIndex++] = [
        htmlspecialcharsbx($arSection["~NAME"]),
        (!empty($arSection["~SECTION_PAGE_URL"]) ? $arSection["~SECTION_PAGE_URL"] : "javascript:void(0);"),
        $arResult["ELEMENT_LINKS"][$arSection["ID"]],
        [
            "FROM_IBLOCK" => true,
            "IS_PARENT" => false,
            "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
            "HAS_COLS" => $arSection["HAS_COLS"],
            "COL" => $arSection["COL"],
            "TYPE" => $arSection["UF_MENU_TYPE"],
            "DETAIL_PICTURE" => $arSection["DETAIL_PICTURE"],
        ],
    ];
}

array_splice($aMenuLinks, 0, 0, $aMenuLinksNew);
?>
